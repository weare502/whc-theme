<?php 
/*
* Template to display the outpatient schedule
*/
?>

<table class="outpatient-schedule">
	<thead>
		<tr>
			<td>Specialty</td>
			<td>Providers</td>
			<td>Clinic Dates</td>
			<td>For Scheduling</td>
		</tr>
	</thead>
	<tbody>

		<?php while( have_rows( 'outpatient_schedule' ) ) : the_row();
			
			$doctor = get_sub_field( 'doctor' );

			//var_dump($doctor);
			$specialty = get_field( 'doc_specialty', $doctor[0]->ID );
			$phone = get_sub_field( 'phone' );
			$dates = get_sub_field( 'dates' );
			$docs = '';

			foreach ( $doctor as $doc ){
				$docs .= "<p><span class='blue' href='" . get_permalink( $doc->ID ) . "'>" . $doc->post_title . "</span><br><span class='small'>" . get_field('doc_full_time_organization', $doc->ID ) . "</span></p>";
			}

		?>

		<tr>
			<td class="specialty"><?php echo $specialty; ?></td>
			<td class="doctors"><?php echo $docs; ?></td>
			<td class="clinic-dates"><?php echo $dates; ?></td>
			<td class="phone"><?php echo $phone; ?></td>
		</tr>

		<?php endwhile; ?>

	</tbody>
</table>