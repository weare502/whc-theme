<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Wamego Health Center
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Cantarell:400,700' rel='stylesheet' type='text/css'>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'whc' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
		<?php $logo = get_field('options_site_logo', 'options'); ?>

		<?php if ( $logo != '' || null) : $logo = $logo['sizes']['logo']; ?>
			<h1 title="<?php bloginfo('name'); ?>" class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo $logo; ?>" id="site-logo" alt="<?php bloginfo('name'); ?>" /></a></h1>
		<?php else : ?>
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
		<?php endif; ?>
		</div>

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<button class="menu-toggle"><?php _e( 'Menu', 'whc' ); ?></button>
			<?php wp_nav_menu( array( 'theme_location' => 'secondary', 'menu_id' => 'secondary-menu' ) ); ?>
			<div class="nav-bar">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'container' => '', 'depth' => 1 ) ); ?>
			<form action="/" method="GET" id="nav-search">
				<span>
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					 width="10px" height="15px" viewBox="0 0 12.6 19.9" enable-background="new 0 0 12.6 19.9" xml:space="preserve">
				<circle fill="none" stroke="#FFFFFF" stroke-width="1.5" stroke-miterlimit="10" cx="6.2" cy="6.2" r="5.5"/>
				<line fill="none" stroke="#FFFFFF" stroke-width="1.5" stroke-miterlimit="10" x1="8" y1="11" x2="11.9" y2="19.5"/>
				</svg></span>
<input type="text" name="s" class="nav-search" placeholder="Search" />
				<input type="submit" class="screen-reader-text" />
			</form>
			</div>
		</nav><!-- #site-navigation -->
		<address class="address">
			711 Genn Dr. <br/>
			Wamego, KS 66547<br/>
			<a href="tel:785-456-2295">785-456-2295</a>
		</address>
	</header><!-- #masthead -->

	<div id="content" class="site-content">