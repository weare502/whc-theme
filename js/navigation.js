/**
 * navigation.js
 *
 * Handles toggling the navigation menu for small screens and moving content margin.
 */
(function($){
	
	// vars
	var $button, $mainMenu, $secondaryMenu, $menuWrap;
	
	$button = $('.menu-toggle');
	$menuWrap = $('#site-navigation');
	$mainMenu = $('.nav-bar');
	$secondaryMenu = $('#secondary-menu');
	
	$button.on('click', function(){
		
		if ( $menuWrap.hasClass('toggled') ){
			$button.html('Menu');
			$mainMenu.removeClass('shown');
			$secondaryMenu.removeClass('shown');
			$menuWrap.removeClass('toggled');
		} else {
			$menuWrap.addClass('toggled');
			$button.html('&times;');
			$mainMenu.addClass('shown');
			$secondaryMenu.addClass('shown');
		}

	});
	
	// Set correct margins on #content for small screens.
	
	// vars
	var $header, $content, $h, $window;
	
	$window = $(window);
	$header = $('#masthead');
	$content = $('#content');
		
	$window.resize(function(){
		
		$h = $header.height();
		$content.css( 'margin-top', $h + 24 );

	});
	
	$window.on('load', function(){	
		if ( $window.width() < 640 ){
			$h = $header.height();
			$content.css( 'margin-top', $h + 24 );
		}
	});

	$('.open-popup-link').magnificPopup({
	  type:'inline',
	  midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
	});

	
})(jQuery);