<?php 
	$parent = $post->post_parent; //get_post_ancestors( $post->ID )[0];
	$current_page = $post->ID;
	// $grandparent = get_post_ancestors( $parent );

	if ( $parent == 0 ) :
		$args =  array(
			'post_type' => 'page',
			'post_parent' => $current_page,
			'orderby' => 'post_title',
			'order' => 'ASC',
			'posts_per_page' => -1
		);

	else :
	
		$args = array(
				'post_type' => 'page',
				'post_parent' => $parent,
				'orderby' => 'post_title',
				'order' => 'ASC',
				'posts_per_page' => -1
			);

	endif;

	$query = new WP_Query($args);

?>


<?php // the_breadcrumb(); ?>

<nav class="page-nav">

	<?php while ( $query->have_posts() ) : $query->the_post();
		
		if ($post->ID == $current_page) :
			echo '<a href="' . get_permalink() . '" class="current">' . get_the_title() . '</a>';
		else :
			echo '<a href="' . get_permalink() . '">' . get_the_title() . '</a>';
		endif;

		$args = array(
				'child_of' => $post->ID,
				'sort_order' => 'ASC',
				'sort_column' => 'post_title'
			);

	 	$children = get_pages($args);
	 	$current = $post->ID;

	 	if ( !empty( $children ) ) :

	 		foreach ( $children as $child ) :

	 			$child = $child->ID;

	 			$nest = get_post_ancestors($child);

	 			echo '<a href="' . get_permalink($child) . '">';
	 			foreach ($nest as $sep) {
	 				
	 				if ( $sep == $current){
	 					echo '&emsp;'; // echo '&mdash;'; 
	 					break;
	 				}
	 				else {
	 					echo '&emsp;'; //echo '&mdash;'; 
	 				}

	 			}
	 			echo '&emsp;' . get_the_title($child) . '</a>';

	 		endforeach;

	 	endif;

	endwhile; wp_reset_postdata(); ?>
	
</nav>