<div class="single-doctor">
	<?php if ( ! empty( get_field( 'doc_headshot' ) ) ) : 
			$image = get_field( 'doc_headshot' ); 
	?>
		<img class="doctor-headshot" src="<?php echo $image['sizes']['medium']; ?>" alt="<?php the_title(); ?>" />
	<?php endif; ?>
	<?php if ( ! empty( get_field( 'doc_specialty' ) ) ) : ?>
		<p><strong>Specialty:</strong> <?php the_field( 'doc_specialty' ); ?></p>
	<?php endif; ?>

	<?php if ( ! empty( get_field( 'doc_credentials' ) ) ) : ?>
		<p><strong>Credentials: </strong><?php the_field( 'doc_credentials' ); ?></p>
	<?php endif; ?>

	<?php if ( ! empty( get_field( 'doc_phone_number' ) ) ) : ?>
		<strong>Phone: </strong><a href="tel:<?php the_field( 'doc_phone_number' ); ?>"><?php the_field( 'doc_phone_number' ); ?></a><br>
	<?php endif; ?>

	<?php if ( ! empty( get_field( 'doc_offices' ) ) ) : ?>
		<br><strong>Offices</strong><address><?php the_field( 'doc_offices' ); ?></address><br>
	<?php endif; ?>

	<?php if ( ! empty( get_field( 'doc_full_time_organization' ) ) ) : ?>
		<p><strong>Full Time Organization: </strong><?php the_field( 'doc_full_time_organization' ); ?></p>
	<?php endif; ?>

	<?php if ( ! empty( get_field( 'doc_medical_school' ) ) ) : ?>
		<p><strong>Medical School: </strong><?php the_field( 'doc_medical_school' ); ?></p>
	<?php endif; ?>

	
</div>