<?php
/**
 * Template file for the "Doctors" field group in ACF
 */

if ( have_rows( 'doctors' ) ) : ?>

	<section id="doctors">

	<?php while ( have_rows( 'doctors' ) ) : the_row(); ?>

		<?php $image = get_sub_field( 'image' ); ?>

		<article class="doctor clearfix">
			
			<?php if ( $image ) : ?>
				<img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
			<?php endif; ?>

			<h2><?php the_sub_field( 'name' ); ?></h2>

			<div class="title"><?php the_sub_field( 'title' ); ?></div>

			<p class="bio"><?php the_sub_field( 'short_bio' ); ?></p>

			<?php if ( get_sub_field( 'accepting_patients' ) ) : ?>

				<p class="accepting-patients"><span class="callout">Now Accepting Patients!</span></p>

			<?php endif; ?>

		</article>

	<?php endwhile; ?>

	</section>

<?php endif; ?>