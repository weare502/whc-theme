<?php
/**
 * Wamego Health Center functions and definitions
 *
 * @package Wamego Health Center
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'whc_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function whc_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Wamego Health Center, use a find and replace
	 * to change 'whc' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'whc', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'whc' ),
		'secondary' => __('Secondary Menu', 'whc'),
		'footer' => __('Footer Menu', 'whc')
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	// add_theme_support( 'post-formats', array(
	// 	'aside', 'image', 'video', 'quote', 'link',
	// ) );

	// Setup the WordPress core custom background feature.
	// add_theme_support( 'custom-background', apply_filters( 'whc_custom_background_args', array(
	// 	'default-color' => 'ffffff',
	// 	'default-image' => '',
	// ) ) );

	add_image_size( 'logo', 650, 300 );
	add_image_size( 'home_page_hero', 1200, 400, array( 'center', 'center') );
	add_image_size( 'news', 520, 240, array( 'center', 'center') );
}
endif; // whc_setup
add_action( 'after_setup_theme', 'whc_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function whc_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'whc' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );

	register_sidebar( array(
		'name'          => __( 'Pages Sidebar', 'whc' ),
		'id'            => 'sidebar-2',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer Sidebar 1', 'whc' ),
		'id'            => 'footer-sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer Sidebar 2', 'whc' ),
		'id'            => 'footer-sidebar-2',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer Sidebar 3', 'whc' ),
		'id'            => 'footer-sidebar-3',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'whc_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function whc_scripts() {
	wp_enqueue_style( 'whc-style', get_stylesheet_directory_uri() .'/css/style.css', array(), '20150102' );

	wp_enqueue_style( 'whc-magnific-css', get_stylesheet_directory_uri() .'/bower_components/magnific-popup/dist/magnific-popup.css', array(), '20150101' );

	wp_enqueue_script( 'whc-magnific-popup', get_template_directory_uri() . '/bower_components/magnific-popup/dist/jquery.magnific-popup.min.js', array( 'jquery' ), '20120207', true );

	wp_enqueue_script( 'whc-navigation', get_template_directory_uri() . '/js/navigation.js', array( 'jquery' ), '20120207', true );

	wp_enqueue_script( 'whc-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array( 'jquery' ), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'whc_scripts' );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


if (function_exists('acf_add_options_page')){
	$args = array(
			'page_title' => 'Site Options',		
		);
	acf_add_options_page($args);
}
/**
 * Return a formatted breadcrumb trail of links
 * @return str
 */
function the_breadcrumb(){ ?>
	<div class="breadcrumbs">
		<p>
			<a href="/">Home</a>
			&raquo;

	<?php $anc = array_reverse(get_post_ancestors( $post->ID ));
	    foreach ( $anc as $ancestor ) : ?>

				<a itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="url" href="<?php echo get_permalink($ancestor); ?>">
					<span itemprop="title"><?php echo get_the_title($ancestor); ?></span>
				</a>
				&raquo;

		<?php endforeach; ?>

			<span><?php echo the_title(); ?></span>
		</p>
	</div>
<?php }

/**
* Registers a new post type
* @uses $wp_post_types Inserts new post type object into the list
*
* @param string  Post type key, must not exceed 20 characters
* @param array|string  See optional args description above.
* @return object|WP_Error the registered post type object, or an error object
*/
function whc_register_doctors() {

	$labels = array(
		'name'                => __( 'Doctors', 'whc' ),
		'singular_name'       => __( 'Doctor', 'whc' ),
		'add_new'             => _x( 'Add New Doctor', 'whc', 'whc' ),
		'add_new_item'        => __( 'Add New Doctor', 'whc' ),
		'edit_item'           => __( 'Edit Doctor', 'whc' ),
		'new_item'            => __( 'New Doctor', 'whc' ),
		'view_item'           => __( 'View Doctor', 'whc' ),
		'search_items'        => __( 'Search Doctors', 'whc' ),
		'not_found'           => __( 'No Doctors found', 'whc' ),
		'not_found_in_trash'  => __( 'No Doctors found in Trash', 'whc' ),
		'parent_item_colon'   => __( 'Parent Doctor:', 'whc' ),
		'menu_name'           => __( 'Doctors', 'whc' ),
	);

	$args = array(
		'labels'                   => $labels,
		'hierarchical'        => false,
		'description'         => 'Doctors who work for WHC',
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-id-alt',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => array( 'slug' => 'doctors' ),
		'capability_type'     => 'post',
		'supports'            => array(
			'title'
			)
	);

	register_post_type( 'whc_doctors', $args );
}

add_action( 'init', 'whc_register_doctors' );

function whc_new_excerpt_more($more) {
     global $post;

     if ( is_front_page() )
     	return;
     
	return '<br><br><a class="moretag" href="'. get_permalink($post->ID) . '"> Read the full article...</a>';
}
add_filter('excerpt_more', 'whc_new_excerpt_more');







