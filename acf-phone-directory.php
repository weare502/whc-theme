<?php 
/**
 * Shows the tables for the hospital phone directory
 */
?>
<?php if ( have_rows( 'directory_departments' ) ) : ?>

	<div id="phone-directory">

		<?php while ( have_rows( 'directory_departments' ) ) : the_row(); ?>

			<h3><?php the_sub_field( 'department_name' ); ?></h3>

			<?php if ( have_rows( 'department_contacts' ) ) : ?>
				
				<table>

					<thead>
						<tr>
							<td>Name</td>
							<td>Title</td>
							<td>Phone</td>
							<td>Fax</td>
						</tr>
					</thead>

					<?php while( have_rows( 'department_contacts' ) ) : the_row(); ?>
						
						<tr>
							<td><?php the_sub_field( 'name' ); ?></td>
							<td><?php the_sub_field( 'title' ); ?></td>
							<td><?php the_sub_field( 'phone' ); ?></td>
							<td><?php the_sub_field( 'fax' ); ?></td>
						</tr>

					<?php endwhile; ?>

				</table>

			<?php endif; ?>

		<?php endwhile; ?>

	</div>

<?php endif; ?>