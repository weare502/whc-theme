<?php
/**
 * @package Wamego Health Center
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<?php if ( has_post_thumbnail() ) : ?>
			<?php the_post_thumbnail( 'large' ); ?>
		<?php endif; ?>

		<div class="entry-meta">
			<?php if ( ! is_singular( 'whc_doctors' ) ) whc_posted_on(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>

		<?php if ( is_singular( 'whc_doctors' ) ) {
				require( 'doctors.php' );
			}  ?>

		<?php
		if ( ! is_singular( 'whc_doctors' ) ){
				wp_link_pages( array(
					'before' => '<div class="page-links">' . __( 'Pages:', 'whc' ),
					'after'  => '</div>',
				) );
			}
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php whc_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
