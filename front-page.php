<?php
/**
 * The template for displaying the front page.
 *
 * @package Wamego Health Center
 */

get_header(); ?>

<?php 
	$hero = get_field('hero_image');
	if ( !empty($hero) ) : 
		$size = 'home_page_hero';
		$url = $hero['sizes'][$size];
		$width = $hero['sizes'][$size . '-width'];
		$height = $hero['sizes'][$size . '-height'];
?>
		<img src="<?php echo $url; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
<?php endif; ?>

<div class="hero-cta">
	<span>
		<a href="#choose-chart" class="chart open-popup-link">
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="42px" height="61px" viewBox="0 0 42 61" enable-background="new 0 0 42 61" xml:space="preserve">
<g>
	<polyline fill="none" stroke="#0F56C9" stroke-width="2" stroke-miterlimit="10" points="36,12 41,12 41,60 1,60 1,12 9,12 	"/>
	<line fill="none" stroke="#0F56C9" stroke-width="2" stroke-miterlimit="10" x1="10" y1="36" x2="34" y2="36"/>
	<line fill="none" stroke="#0F56C9" stroke-width="2" stroke-miterlimit="10" x1="10" y1="29" x2="34" y2="29"/>
	<line fill="none" stroke="#0F56C9" stroke-width="2" stroke-miterlimit="10" x1="10" y1="43" x2="34" y2="43"/>
	<line fill="none" stroke="#0F56C9" stroke-width="2" stroke-miterlimit="10" x1="10" y1="50" x2="27" y2="50"/>
	<path fill="none" stroke="#0F56C9" stroke-width="2" stroke-miterlimit="10" d="M24,17h-3h-7.9c-1,0-2.1-2.4-2.1-3.3V5.5
		C11,4.6,14.1,5,15,5h2.6c0.2-3,2-4,4-4H22h1h0.4c2,0,3.8,1,4,4H30c1,0,4-0.4,4,0.5v8.3c0,0.8-1.2,3.3-2.1,3.3H24"/>
</g>
<rect id="_x3C_Slice_x3E_" fill="none" width="42" height="61"/>
</svg><span>Access My Chart</span></a>
		<a href="<?php the_field('doctor_url'); ?>">
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="58.2px" height="56.3px" viewBox="0 0 58.2 56.3" enable-background="new 0 0 58.2 56.3" xml:space="preserve">
<g>
	<path fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#0F56C9" stroke-width="2.4016" stroke-miterlimit="10" d="
		M0.7,55.3c1.6-1,3.1-2.2,4.8-2.9c4.4-1.8,9-3.4,13.4-5.1c0,0,0.1,0,0.1,0c2.6-0.9,3-1.5,3-4.2c0-0.7,0-1.3,0-2
		c0.1-1.6-0.3-3-1.4-4.3c-1.5-1.8-1.9-4-2.3-6.1c-0.2-1.2-0.7-2.3-1.2-3.5c-0.7-1.6-1-3.3-1.4-5c-0.1-0.3,0-0.6,0.1-0.9
		c0.8-2,0.3-3.9,0.1-5.9c-0.6-5.3,1.6-9.3,6-12c3.2-2,6.9-2.4,10.6-2c3.3,0.4,6.3,1.6,8.6,4.1c2.6,2.8,3.9,6.1,3.3,10
		c-0.2,1.2-0.3,2.4-0.3,3.6c0,0.4,0.1,0.9,0.2,1.3c0.1,1,0.3,2,0.1,3c-0.3,1.7-0.8,3.3-1.6,4.8c-0.5,0.9-0.6,2.1-0.9,3.1
		c-0.5,2.3-1.2,4.5-2.7,6.4c-0.4,0.5-0.7,1.3-0.7,1.9c-0.1,1.5,0,3.1-0.1,4.6c0,1.5,0.8,2.3,2.1,2.8c1.5,0.5,2.9,1,4.4,1.5
		c4.1,1.4,8.1,2.9,11.9,4.9c0.3,0.2,0.6,0.4,0.9,0.5"/>
</g>
</svg>
<span>Find a Doctor</span></a>
		<a href="<?php the_field('appointment_url'); ?>">
<svg width="37px" height="60px" viewBox="0 0 37 60" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
        <g id="$" sketch:type="MSLayerGroup" transform="translate(0.000000, -10.000000)" font-size="67.5712719" font-family="Helvetica Neue" fill="#0F56C9" font-weight="100">
            <text sketch:type="MSTextLayer">
                <tspan x="0" y="63" fill="#0F56C9">$</tspan>
            </text>
        </g>
    </g>
</svg><span>Pay My Bill</span></a>
	</span>
</div>
<div id="choose-chart" class="mfp-hide white-popup">
	<img src='<?php echo get_stylesheet_directory_uri(); ?>/chart.svg' class="chart-img" />
	<div class="popup-title">Choose Your Chart</div>
	<a href="<?php the_field('hospital_chart_url') ?>" target="_blank" class="button">Hospital Chart</a>
	<a href="<?php the_field('clinic_chart_url') ?>" target="_blank" class="button">Clinic Chart</a>
</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php // get_template_part( 'content', 'page' ); ?>

			<?php endwhile; // end of the loop. ?>
			
			<section class ="news">
				<!-- <h4>News</h4> -->

				<?php $args = array(
					'posts_per_page' => 2,
					'post__in'  => get_option( 'sticky_posts' ),
					'ignore_sticky_posts' => 1
				);
				$query = new WP_Query( $args );

				if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

						<a href="<?php the_permalink(); ?>" class="image-link"><?php the_post_thumbnail('news'); ?></a>
						<h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
						<p><?php the_excerpt(); ?></p>
						<a href="<?php the_permalink(); ?>" class="button">Read More</a>

					</article>

				<?php endwhile; endif; wp_reset_postdata(); ?>

			</section>

		</main><!-- #main -->
	</div><!-- #primary -->
<script type="text/javascript">
// var $chart = jQuery('.chart');
// jQuery( $chart ).on('click', function( e ){
// 	swal({   
// 		title: "Select your chart",      
// 		showCancelButton: true,
// 		allowOutsideClick: true,
// 		imageUrl: '<?php echo get_stylesheet_directory_uri(); ?>/chart.svg',
// 		cancelButtonText: 'Wamego Family Clinic',    
// 		confirmButtonText: "Wamego Health Center",
// 		confirmButtonColor: '#0F56C9',
// 		cancelButtonColor: '#0F56C9',   
// 		closeOnConfirm: false,
// 		closeOnCancel: false 
// 	}, 
// 		function(isConfirm){   
// 			if(isConfirm){
// 				window.location.assign("https://wchportal.via-christi.org/");	
// 			} else {
// 				window.location.assign("https://5796.iqhealth.com/");	
// 			}
// 		}
// 	);
// 	e.preventDefault();
// });
</script>
<?php get_footer(); ?>
