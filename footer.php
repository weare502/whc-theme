<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Wamego Health Center
 */
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<!-- <section>
			<aside>
				<h6>Contact</h6>
				<address>
					711 Genn Dr. <br/>
					Wamego, KS 66547<br/>
					<a href="tel:785-456-2295">785-456-2295</a>
				</address>
			</aside>
			<aside>
				<ul>
					<li>Something</li>
					<li>Another Thing</li>
					<li>Hoorah! Jk LOL</li>
					<li>Blunderbuss</li>
					<li>Zimbabawe</li>
				</ul>
			</aside>
			<aside>
				<ul>
					<li>Something</li>
					<li>Another Thing</li>
					<li>Zimbabawe</li>
				</ul>
			</aside>
			<aside>
				<ul>
					<li>Another Thing</li>
					<li>Hoorah! Jk LOL</li>
					<li>Blunderbuss</li>
					<li>Zimbabawe</li>
				</ul>
			</aside>

		</section> -->

		<div class="clear">
			<div class="third"><?php dynamic_sidebar( 'footer-sidebar-1' ); ?></div>
			<div class="third"><?php dynamic_sidebar( 'footer-sidebar-2' ); ?></div>
			<div class="third"><?php dynamic_sidebar( 'footer-sidebar-3' ); ?></div>
		</div>
		<div class="site-info">	
			<?php echo '&copy;&nbsp;' . date('Y') . '&nbsp;Wamego Health Center'; wp_nav_menu( array( 'theme_location' => 'footer' ) ); ?>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<script src="<?php echo get_stylesheet_directory_uri() . '/js/sweet-alert.min.js'; ?>" type="text/javascript"></script>
<?php wp_footer(); ?>

</body>
</html>
