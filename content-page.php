<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Wamego Health Center
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<?php the_breadcrumb(); ?>
	
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'whc' ),
				'after'  => '</div>',
			) );
		?>
		<?php if ( is_page( 133 ) ){
			get_template_part( 'acf', 'doctors' );
		} ?>

		<?php if ( is_page( 79 ) ){
			get_template_part( 'acf', 'phone-directory' );
		} ?>

		<?php if ( have_rows('outpatient_schedule') ){
			get_template_part('acf', 'outpatient-schedule');
		} ?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php edit_post_link( __( 'Edit', 'whc' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
